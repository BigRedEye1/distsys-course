# Техподдержка

## Как репортить ошибки

Если вы пишете в чат поддержки, то, пожалуйста, приложите через [pastebin](https://pastebin.com/) полный выхлоп клиента вместе с командой и диагностической информацией: 

```
rlipovsky@f6bc43481922:/workspace/distsys-course$ clippy hi
--------------------------------------------------------------------------------
Howdy, rlipovsky!

Command running: ['hi'], cwd: /workspace/distsys-course
Platform: Linux-4.19.76-linuxkit-x86_64-with-glibc2.29
C++ compiler: /usr/bin/clang++-10 (clang version 10.0.0-4ubuntu1)
Python: 3.8.5, CPython, /workspace/distsys-course/client/venv/bin/python
Repository root directory: /workspace/distsys-course
Git current commit: d47cf9a3c55f6bb0be274c599621a5a1b396c579

...
```

## Gitlab.com

https://twitter.com/gitlabstatus