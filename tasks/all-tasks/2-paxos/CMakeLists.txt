cmake_minimum_required(VERSION 3.5)

begin_task()
add_task_library(consensus whirl-v1)
add_task_library(paxos whirl-v1)
add_task_test_dir(tests-1)
end_task()
