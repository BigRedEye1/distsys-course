#pragma once

#include <paxos/node/proposal.hpp>
#include <paxos/node/protocol.hpp>

#include <whirl/node/logging.hpp>
#include <whirl/node/peer_base.hpp>
#include <whirl/rpc/use/service_base.hpp>

#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>
#include <await/futures/combine/quorum.hpp>

namespace paxos {

using whirl::NodeServices;
using whirl::PeerBase;
using whirl::rpc::ServiceBase;

using await::fibers::Await;
using await::futures::Future;
using await::futures::Quorum;

////////////////////////////////////////////////////////////////////////////////

// Proposer role / RPC service

class Proposer : public ServiceBase<Proposer>, public PeerBase {
 public:
  Proposer(NodeServices services) : PeerBase(std::move(services)) {
  }

 protected:
  void RegisterRPCMethods() {
    RPC_REGISTER_METHOD(Propose);
  }

  Value Propose(Value input) {
    return input;  // Violates agreement property
    // return 0;  // Violates validity property
  }
};

}  // namespace paxos
