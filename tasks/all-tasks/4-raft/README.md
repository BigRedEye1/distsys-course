# KV + RAFT = ♥

## Пролог

_After struggling with Paxos ourselves, we set out to find a new consensus algorithm that could provide a better foundation for system building and education_.

[Источник](https://raft.github.io/raft.pdf)

## Задача

Реализуйте отказоустойчивое линеаризуемое KV хранилище с помощью RSM + RAFT.

## Отказоустойчивость

Система должна переживать:

- Рестарты узлов
- Партишены сети
- Отказ произвольного меньшинства узлов

## Модель согласованности

[Линеаризуемость](https://jepsen.io/consistency/models/linearizable)

## RAFT

- [In Search of an Understandable Consensus Algorithm](https://raft.github.io/raft.pdf) – спецификация на 4-ой странице
- [RAFT lecture](https://www.youtube.com/watch?v=YbZ3zDzDnrw), [слайды](https://ongardie.net/static/raft/userstudy/raft.pdf)
- [Спецификация на TLA+](https://github.com/ongardie/raft.tla/blob/master/raft.tla)

#### Реализация

- [LogCabin](https://github.com/logcabin/logcabin) – референсная реализация алгоритма от автора RAFT. 
- [Implementing RAFT](https://eli.thegreenplace.net/2020/implementing-raft-part-0-introduction/) – в этой реализации достаточно ошибок, попробуйте их обнаружить!
- [Students' Guide to Raft](https://thesquareplanet.com/blog/students-guide-to-raft/)

## Фреймворк

// TODO

## Чего от меня хотят?

Реализовать метод [`RaftRSM::Execute`](rsm/raft/raft.cpp).

Никакие другие файлы менять не требуется.
