#pragma once

#include <rsm/raft/log_entry.hpp>

#include <whirl/services/local_storage.hpp>

#include <string>
#include <memory>

namespace raft {

// Indexed from 1!

class PersistentLog {
 public:
  PersistentLog(whirl::ILocalStorageBackendPtr storage,
                const std::string& name);

  size_t Length() const;

  // Precondition: 0 < index <= Length()
  LogEntry Read(size_t index) const;

  // Precondition: 0 < index <= Length()
  size_t Term(size_t index) const;

  // Or 0 is Length() == 0
  size_t LastLogTerm() const;

  void Append(const LogEntries& entries, size_t entries_start_index = 0);

  // Persist appends
  void Persist();

  // Truncate and persist new length
  void Truncate(size_t target_length);

  // For debugging
  std::string Print(bool with_commands = false);

 private:
  void Append(const LogEntry& entry);

  void Load();
  void PersistLength();

  static std::string MakeKey(size_t index);
  static std::string MakeLogName(const std::string& name);

 private:
  size_t length_;

  whirl::LocalKVStorage<LogEntry> data_;
  whirl::LocalStorage meta_;
};

}  // namespace raft
