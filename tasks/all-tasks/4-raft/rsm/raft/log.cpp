#include <rsm/raft/log.hpp>

#include <whirl/cereal/serialize.hpp>

#include <wheels/support/assert.hpp>

namespace raft {

using whirl::ILocalStorageBackendPtr;

PersistentLog::PersistentLog(ILocalStorageBackendPtr storage,
                             const std::string& name)
    : data_(storage, MakeLogName(name)), meta_(storage, MakeLogName(name)) {
  Load();
}

size_t PersistentLog::Length() const {
  return length_;
}

LogEntry PersistentLog::Read(size_t index) const {
  WHEELS_VERIFY(index <= Length(),
                "Log index = " << index << " > length = " << length_);
  return data_.Get(MakeKey(index));
}

size_t PersistentLog::Term(size_t index) const {
  return Read(index).term;
}

size_t PersistentLog::LastLogTerm() const {
  if (length_ == 0) {
    return 0;
  }
  return Read(length_).term;
}

void PersistentLog::Append(const LogEntries& entries,
                           size_t entries_start_index) {
  for (size_t i = entries_start_index; i < entries.size(); ++i) {
    Append(entries[i]);
  }
  PersistLength();
}

void PersistentLog::Persist() {
  // Append currently persist changes
}

void PersistentLog::Truncate(size_t target_length) {
  length_ = target_length;
  PersistLength();
}

std::string PersistentLog::Print(bool with_commands) {
  if (length_ == 0) {
    return "[]";
  }

  std::stringstream out;

  out << "[";
  for (size_t i = 1; i <= length_; ++i) {
    const auto entry = Read(i);
    out << entry.term;
    if (with_commands) {
      out << "/" << entry.command;
    }
    if (i < length_) {
      out << ", ";
    }
  }
  out << "] (length = " << length_ << ")";

  return out.str();
}

void PersistentLog::Append(const LogEntry& entry) {
  size_t slot = ++length_;
  data_.Set(MakeKey(slot), entry);
}

void PersistentLog::Load() {
  length_ = meta_.LoadOr("length", 0);
}

void PersistentLog::PersistLength() {
  meta_.Store("length", length_);
}

std::string PersistentLog::MakeKey(size_t index) {
  return std::to_string(index);
}

std::string PersistentLog::MakeLogName(const std::string& name) {
  return std::string("log-") + name;
}

}  // namespace raft
