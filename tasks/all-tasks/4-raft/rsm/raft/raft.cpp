#include <rsm/raft/raft.hpp>

#include <rsm/raft/peer_base.hpp>
#include <rsm/raft/proto.hpp>
#include <rsm/raft/log.hpp>
#include <rsm/raft/nop.hpp>

#include <whirl/node/logging.hpp>

#include <whirl/rpc/use/service_base.hpp>

#include <whirl/services/local_storage.hpp>

#include <whirl/time.hpp>

#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>
#include <await/fibers/sync/mutex.hpp>
#include <await/fibers/sync/channel.hpp>

#include <cstdlib>
#include <algorithm>
#include <map>

namespace raft {

using whirl::Duration;
using whirl::LocalStorage;
using whirl::rpc::ServiceBase;
using whirl::NodeServices;

using namespace whirl::time_literals;

using await::fibers::Await;
using await::fibers::Channel;
using await::fibers::Mutex;
using await::futures::Future;
using await::futures::Promise;

////////////////////////////////////////////////////////////////////////////////

enum class NodeState {
  Follower = 1, // Do not format
  Candidate = 2,
  Leader = 3
};

////////////////////////////////////////////////////////////////////////////////

class RaftRSM : public IReplicatedStateMachine,
                public PeerBase,
                public ServiceBase<RaftRSM>,
                public std::enable_shared_from_this<RaftRSM> {
 public:
  RaftRSM(std::string name, IStateMachinePtr state_machine,
          NodeServices runtime)
      : PeerBase(std::move(runtime)),
        name_(std::move(name)),
        state_machine_(std::move(state_machine)),
        log_(StorageBackend(), name),
        local_storage_(StorageBackend(), "raft_node") {
  }

  Future<RSMResponse> Execute(Command command) override {
    auto [future, promise] = await::futures::MakeContract<RSMResponse>();

    auto guard = mutex_.Guard();

    if (!AmILeader()) {
      if (leader_id_.has_value()) {
        NODE_LOG("Redirect command {} to leader {}", command, *leader_id_);
        std::move(promise).SetValue(RedirectToLeader{*leader_id_});
      } else {
        NODE_LOG("Reject command {}", command);
        std::move(promise).SetValue(NotALeader{});
      }
      return std::move(future);
    }

    // Leader!
    AppendToLog(command);

    return std::move(future);
  };

  void Start() override {
    PeerBase::ConnectToPeers();

    InitVolatileState();

    auto self = shared_from_this();
    RPCServer()->RegisterService("RaftNode", self);

    Spawn([this]() { ApplyCommittedCommands(); });

    BecomeFollower(/*term=*/1);
  }

 protected:
  // RPC

  void RegisterRPCMethods() override {
    RPC_REGISTER_METHOD(RequestVote);
    RPC_REGISTER_METHOD(AppendEntries);
  }

  // Leader election

  proto::RequestVote::Response RequestVote(
      proto::RequestVote::Request request) {
    auto guard = mutex_.Guard();

    proto::RequestVote::Response response;
    // Your code goes here
    return response;
  }

  // Replication

  proto::AppendEntries::Response AppendEntries(
      proto::AppendEntries::Request request) {
    auto guard = mutex_.Guard();

    proto::AppendEntries::Response response;
    // Your code goes here
    return response;
  }

 private:
  // State changes

  // With mutex
  void BecomeFollower(size_t term) {
    // Cancel pending requests?
    // Your code goes here
  }

  // With mutex
  void BecomeCandidate() {
    // Cancel pending requests?
    // Your code goes here
  }

  // With mutex
  void BecomeLeader() {
    // Your code goes here
  }

 private:
  // Fibers: replication, leader election, etc
  // Each fiber is bounded to specific term

  // Apply committed commands to local state machine
  void ApplyCommittedCommands() {
    // See RAFT spec
    size_t last_applied = 0;

    // To appear in simulation log
    await::fibers::self::SetName("applier");
    // Your code goes here
  }

  // Election timer for follower / candidate
  void RunElectionTimer(size_t term) {
    // Your code goes here
  }

  // Leader routine
  void Replicate(size_t term) {
    // Replicate to peers (See PeerBase::PeerIds())
  }

  // Invoke RequestVote RPC on peer_id
  void DoRequestVote(ServerId peer_id, size_t term) {
    proto::RequestVote::Request request;
    // Your code goes here
  }

  // Invoke AppendEntries RPC on peer_id
  void DoAppendEntries(ServerId peer_id, size_t term) {
    proto::AppendEntries::Request request;
    // Your code goes here
  }

 private:
  auto ElectionTimeout() const {
    static const auto kElectionTimeout = 1500_jiffies;

    // Randomize to prevent livelocks!
    return kElectionTimeout;
  }

  // Work with local state
  // All methods should be invoked with mutex_ locked

  bool AmILeader() const {
    return state_ == NodeState::Leader;
  }

  void AppendToLog(Command command) {
    // Your code goes here
  }

  size_t ComputeCommittedPrefix() {
    return 0;  // Your code goes here
    // Do not forget about RAFT commit rule
  }

  void ResetPeersProgress() {
    // See RAFT spec
    // next_index_ / match_index_
  }

  void InitVolatileState() {
    // See RAFT spec
  }

  // Use in AppendEntries RPC handler
  void ResetElectionTimer() {
  }

  void UpdateTerm(size_t term) {
    // Your code goes here
  }

  void SetVote(size_t id) {
    // Your code goes here
  }

  void Apply(Command command) {
    // Apply command to state machine
    state_machine_->Apply(command);
  }

 private:
  std::string name_;

  // One mutex to guard them all
  Mutex mutex_;

  // Replicated state
  IStateMachinePtr state_machine_;

  // Persistent state
  PersistentLog log_;
  LocalStorage local_storage_;

  // Replica state
  // See RAFT spec

  size_t current_term_{0};
  NodeState state_;
  std::optional<ServerId> leader_id_;
  std::optional<ServerId> voted_for_id_;

  size_t commit_index_{0};

  // peer id -> next log index to replicate
  std::map<ServerId, size_t> next_index_;
  // peer id -> match index
  std::map<ServerId, size_t> match_index_;
};

////////////////////////////////////////////////////////////////////////////////

IReplicatedStateMachinePtr MakeRaftRSM(const std::string& name,
                                       IStateMachinePtr state_machine,
                                       NodeServices runtime) {
  return std::make_shared<RaftRSM>(name, std::move(state_machine),
                                   std::move(runtime));
}

}  // namespace raft
