#pragma once

#include <whirl/cereal/serialize.hpp>

#include <cereal/types/string.hpp>

#include <string>
#include <ostream>

namespace raft {

//////////////////////////////////////////////////////////////////////

using ClientId = std::string;

// RequestId = (client_id, request_index)

struct RequestId {
  ClientId client_id;
  size_t request_index;

  WHIRL_SERIALIZE(client_id, request_index)
};

//////////////////////////////////////////////////////////////////////

// Format: {client_id}--{request_index}
std::ostream& operator<<(std::ostream& out, const RequestId& id);

bool operator==(const RequestId& lhs, const RequestId& rhs);
bool operator!=(const RequestId& lhs, const RequestId& rhs);

bool operator<(const RequestId& lhs, const RequestId& rhs);

}  // namespace raft
