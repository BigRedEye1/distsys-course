#include <rsm/raft/proto.hpp>

#include <wheels/support/preprocessor.hpp>

namespace raft {

namespace proto {

#define PRINT(name) TO_STRING(name) << " = " << request.name

std::ostream& operator<<(std::ostream& out,
                         const RequestVote::Request& request) {
  out << "RequestVote{" << PRINT(term) << ", " << PRINT(candidate_id) << ", "
      << PRINT(last_log_index) << ", " << PRINT(last_log_term) << "}";

  return out;
}

std::ostream& operator<<(std::ostream& out,
                         const AppendEntries::Request& request) {
  out << "AppendEntries{" << PRINT(term) << ", " << PRINT(leader_id) << ", "
      << PRINT(prev_log_index) << ", " << PRINT(prev_log_term) << ", "
      << "entries count = " << request.entries.size() << ", "
      << PRINT(leader_commit_index) << "}";

  return out;
}

}  // namespace proto

}  // namespace raft