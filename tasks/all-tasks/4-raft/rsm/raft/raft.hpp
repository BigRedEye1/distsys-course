#pragma once

#include <rsm/raft/rsm.hpp>
#include <rsm/raft/state_machine.hpp>

#include <whirl/node/services.hpp>

#include <string>

namespace raft {

IReplicatedStateMachinePtr MakeRaftRSM(const std::string& name,
                                       IStateMachinePtr state_machine,
                                       whirl::NodeServices runtime);

}  // namespace raft
