#pragma once

#include <rsm/raft/request_id.hpp>
#include <rsm/raft/bytes.hpp>

#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>

#include <string>
#include <ostream>

namespace raft {

struct Command {
  RequestId id;
  std::string type;
  Bytes request;

  WHIRL_SERIALIZE(id, type, request)
};

std::ostream& operator<<(std::ostream& out, const Command& command);

// Compare by request id
bool operator==(const Command& lhs, const Command& rhs);
bool operator!=(const Command& lhs, const Command& rhs);

}  // namespace raft
