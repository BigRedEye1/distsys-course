#pragma once

#include <rsm/kv/store/types.hpp>

#include <whirl/cereal/serialize.hpp>
#include <whirl/cereal/empty_message.hpp>

#include <cereal/types/string.hpp>

#include <string>
#include <ostream>

namespace kv {

namespace proto {

// KV operations

//////////////////////////////////////////////////////////////////////

struct Set {
  struct Request {
    Key key;
    Value value;

    WHIRL_SERIALIZE(key, value)
  };

  using Response = whirl::EmptyMessage;
};

//////////////////////////////////////////////////////////////////////

struct Get {
  struct Request {
    Key key;

    WHIRL_SERIALIZE(key)
  };

  struct Response {
    Value value;

    WHIRL_SERIALIZE(value)
  };
};

//////////////////////////////////////////////////////////////////////

struct Cas {
  struct Request {
    Key key;
    Value expected_value;
    Value target_value;

    WHIRL_SERIALIZE(key, expected_value, target_value)
  };

  struct Response {
    Value old_value;

    WHIRL_SERIALIZE(old_value)
  };
};

//////////////////////////////////////////////////////////////////////

// Logging

std::ostream& operator<<(std::ostream& out, const Set::Request& set);
std::ostream& operator<<(std::ostream& out, const Get::Request& get);
std::ostream& operator<<(std::ostream& out, const Cas::Request& cas);

}  // namespace proto

}  // namespace kv
