#include <rsm/kv/node/state_machine.hpp>

#include <rsm/kv/proto/proto.hpp>
#include <rsm/raft/nop.hpp>

#include <wheels/support/compiler.hpp>

namespace kv {

Bytes StateMachine::Apply(const Command& command) {
  if (command.type == "Set") {
    // 1) Deserialize operation request
    auto set = command.request.As<proto::Set::Request>();
    // 2) Apply operation
    kv_.Set(set.key, set.value);
    // 3) Serialize operation response
    return Bytes::Serialize(proto::Set::Response{});

  } else if (command.type == "Get") {
    auto get = command.request.As<proto::Get::Request>();
    kv::Value value = kv_.Get(get.key);
    return Bytes::Serialize(proto::Get::Response{value});

  } else if (command.type == "Cas") {
    auto cas = command.request.As<proto::Cas::Request>();
    kv::Value old_value =
        kv_.Cas(cas.key, cas.expected_value, cas.target_value);
    return Bytes::Serialize(proto::Cas::Response{old_value});

  } else if (command.type == "Nop") {
    return Bytes::Serialize(raft::Nop::Response{});
  }

  // Supress compiler warning
  WHEELS_UNREACHABLE();
}

}  // namespace kv
