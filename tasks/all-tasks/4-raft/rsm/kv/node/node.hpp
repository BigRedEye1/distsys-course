#pragma once

#include <rsm/kv/node/state_machine.hpp>

#include <rsm/raft/raft.hpp>
#include <rsm/raft/client_service.hpp>

#include <whirl/node/node_base.hpp>

namespace kv {

using whirl::NodeBase;
using whirl::NodeServices;
using whirl::rpc::IServerPtr;

////////////////////////////////////////////////////////////////////////////////

class Node : public NodeBase {
 public:
  Node(NodeServices runtime) : NodeBase(runtime) {
  }

 protected:
  void RegisterRPCServices(const IServerPtr& rpc_server) override {
    auto kv_state_machine = std::make_shared<StateMachine>();
    auto kv_rsm = MakeRaftRSM("kv", kv_state_machine, ThisNodeServices());
    kv_rsm->Start();

    rpc_server->RegisterService(
        "RaftRSM",
        std::make_shared<raft::ClientService>(ThisNodeServices(), kv_rsm));
  }

  void MainThread() override {
    // Do nothing
  }
};

}  // namespace kv
