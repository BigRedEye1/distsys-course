#pragma once

#include <rsm/kv/store/store.hpp>

#include <rsm/raft/state_machine.hpp>

#include <map>

namespace kv {

using raft::Bytes;
using raft::Command;
using raft::IStateMachine;

////////////////////////////////////////////////////////////////////////////////

// State machine implementation for Raft RSM

class StateMachine : public IStateMachine {
 public:
  Bytes Apply(const Command& command) override;

 private:
  TinyKVStore kv_;
};

}  // namespace kv
