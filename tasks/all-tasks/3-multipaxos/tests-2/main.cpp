#include <rsm/kv/client/client.hpp>
#include <rsm/kv/node/node.hpp>
#include <rsm/kv/store/types.hpp>
#include <rsm/kv/model/model.hpp>

#include <whirl/node/node_base.hpp>
#include <whirl/node/logging.hpp>

// Simulation
#include <whirl/matrix/world/world.hpp>
#include <whirl/matrix/client/client.hpp>
#include <whirl/matrix/world/global/vars.hpp>
#include <whirl/matrix/test/random.hpp>
#include <whirl/matrix/test/reporter.hpp>

#include <await/fibers/core/id.hpp>
#include <whirl/rpc/impl/id.hpp>

// Concurrency
#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>
#include <await/futures/combine/quorum.hpp>

#include <cereal/types/string.hpp>
#include <fmt/ostream.h>

#include <random>

using namespace await::fibers;
using namespace whirl;

//////////////////////////////////////////////////////////////////////

class AtomicCounter {
 public:
  AtomicCounter(kv::BlockingClient& client, const std::string& name)
    : client_(client), key_(std::move(name)) {
  }

  // Lock-free
  size_t FetchAdd(size_t d) {
    while (true) {
      size_t current = Get();
      if (Cas(current, current + d) == current) {
        return current;
      }
    }
  }

 private:
  size_t Cas(size_t expected, size_t target) {
    kv::Value prev_value = client_.Cas(key_, ToValue(expected), ToValue(target));
    return FromValue(prev_value);
  }

  size_t Get() {
    return FromValue(client_.Get(key_));
  }

  static size_t FromValue(kv::Value value) {
    if (value == kv::ValueTraits::Default()) {
      return 0;
    }
    return std::stoi(value);
  }

  static kv::Value ToValue(size_t value) {
    if (value == 0) {
      return kv::ValueTraits::Default();
    }
    return std::to_string(value);
  }

 private:
  kv::BlockingClient& client_;
  std::string key_;
};

//////////////////////////////////////////////////////////////////////

class KVClient final : public ClientBase {
 public:
  KVClient(NodeServices runtime) : ClientBase(std::move(runtime)) {
  }

 protected:
  void MainThread() override {
    kv::BlockingClient kv_client{Channel(), GenerateUid()};

    AtomicCounter counter(kv_client, "counter");

    const size_t increments_to_do = GetGlobal<size_t>("increments_per_client");

    for (size_t i = 0; i < increments_to_do; ++i) {
      size_t prev_value = counter.FetchAdd(1);

      GlobalCounter("increments").Increment();
      GlobalCounter("total").Add(prev_value);

      SleepFor(RandomNumber(1, 100));
    }
  }
};

//////////////////////////////////////////////////////////////////////

TestReporter reporter;

//////////////////////////////////////////////////////////////////////

// Seed -> simulation digest
size_t RunSimulation(size_t seed) {
  static const size_t kTimeLimit = 35'000;

  reporter.PrintSimSeed(seed);

  Random random{seed};

  // Simulation parameters

  const size_t replicas = random.Get(3, 5);
  const size_t clients = random.Get(3, 5);
  const size_t increments_per_client = random.Get(2, 3);

  const size_t increments = clients * increments_per_client;

  reporter.DebugLine(
      "Parameters: replicas = {}, clients = {}, increments_per_client = {}",
      replicas, clients, increments_per_client);

  // Reset RPC and fiber ids
  await::fibers::ResetIds();
  whirl::rpc::ResetIds();

  World world{seed};

  // Globals

  world.SetGlobal("increments_per_client", increments_per_client);

  world.InitCounter("increments");
  world.InitCounter("total");

  // Cluster nodes
  auto node = MakeNode<kv::Node>();
  world.AddServers(replicas, node);

  // Clients
  auto client = MakeNode<KVClient>();
  world.AddClients(clients, client);

  // Log
  std::stringstream log;
  world.WriteLogTo(log);

  // Run simulation
  world.Start();

  while (world.GetCounter("increments") < increments &&
      world.TimeElapsed() < kTimeLimit) {
    if (!world.Step()) {
      break;
    }
  }

  // Stop and compute simulation digest
  size_t digest = world.Stop();

  // Print report
  reporter.PrintSimReport(world);

  const bool completed = world.GetCounter("increments") == increments;

  // Failed
  if (!completed) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    reporter.PrintSimLog(log.str());

    if (world.TimeElapsed() >= kTimeLimit) {
      reporter.Fail("Simulation time limit exceeded");
    } else {
      reporter.Fail("Deadlock in simulation");
    }
  }

  const size_t total = world.GetCounter("total");
  const size_t total_expected = increments * (0 + increments - 1) / 2;

  reporter.DebugLine("Total = {}, expected = {}",
      total, total_expected);

  const bool correct = total == total_expected;

  if (!correct) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    // Log
    reporter.PrintSimLog(log.str());
    // History
    reporter.PrintSimHistory<kv::Model>(world.History());
    reporter.Fail("Test invariant violated");
  }

  return digest;
}

void TestDeterminism() {
  static const size_t kSeed = 104107713;

  reporter.PrintLine("Test determinism...");

  size_t digest = RunSimulation(kSeed);

  // Repeat with the same seed
  if (RunSimulation(kSeed) != digest) {
    reporter.Fail("Impl is not deterministic");
  } else {
    reporter.PrintLine("Ok");
  }
}

void RunSimulations(size_t count) {
  std::mt19937 seeds{42};

  reporter.ResetSimCount();
  reporter.PrintLine("Run simulations...");

  for (size_t i = 1; i <= count; ++i) {
    reporter.StartSim();
    RunSimulation(seeds());
    reporter.CompleteSim();
  }
}

int main() {
  TestDeterminism();
  RunSimulations(7'000);

  reporter.Congratulate();

  return 0;
}
