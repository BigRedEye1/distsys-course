#pragma once

#include <rsm/multipaxos/command.hpp>

#include <await/futures/future.hpp>

namespace multipaxos {

using await::futures::Future;

struct IReplicatedStateMachine {
  virtual ~IReplicatedStateMachine() = default;

  virtual void Start() = 0;

  virtual Future<CommandOutput> Execute(Command command) = 0;
};

using IReplicatedStateMachinePtr = std::shared_ptr<IReplicatedStateMachine>;

}  // namespace multipaxos
