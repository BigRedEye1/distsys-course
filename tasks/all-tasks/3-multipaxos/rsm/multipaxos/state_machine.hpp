#pragma once

#include <rsm/multipaxos/command.hpp>

#include <wheels/support/result.hpp>

#include <memory>

namespace multipaxos {

using wheels::Result;

struct IStateMachine {
  virtual ~IStateMachine() = default;

  virtual Result<CommandOutput> Apply(const Command& command) = 0;
};

using IStateMachinePtr = std::shared_ptr<IStateMachine>;

}  // namespace multipaxos
