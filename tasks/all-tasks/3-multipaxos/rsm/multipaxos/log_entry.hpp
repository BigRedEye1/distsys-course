#pragma once

#include <whirl/helpers/serialize.hpp>

#include <cereal/types/optional.hpp>

#include <optional>

namespace multipaxos {

struct LogEntry {
  // TODO: Acceptor state
  int _;  // Remove me

  static LogEntry Empty() {
    return {};  // TODO
  }

  WHIRL_SERIALIZE(_)
};

}  // namespace multipaxos
