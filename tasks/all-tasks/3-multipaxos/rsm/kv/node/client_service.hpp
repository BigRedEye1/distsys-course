#pragma once

#include <rsm/kv/proto/proto.hpp>
#include <rsm/multipaxos/command.hpp>
#include <rsm/multipaxos/multipaxos.hpp>

#include <whirl/rpc/use/service_base.hpp>
#include <whirl/node/node_methods_base.hpp>
#include <whirl/node/logging.hpp>

#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>

namespace kv {

using multipaxos::Command;
using multipaxos::CommandInput;
using multipaxos::CommandOutput;
using multipaxos::IReplicatedStateMachinePtr;

using whirl::NodeMethodsBase;
using whirl::NodeServices;
using whirl::rpc::ServiceBase;

using await::fibers::Await;

class ClientService : public ServiceBase<ClientService>,
                      public NodeMethodsBase {
 public:
  ClientService(NodeServices runtime, IReplicatedStateMachinePtr rsm)
      : NodeMethodsBase(runtime), rsm_(std::move(rsm)) {
  }

  void RegisterRPCMethods() override {
    RPC_REGISTER_METHOD(Set);
    RPC_REGISTER_METHOD(Get);
    RPC_REGISTER_METHOD(Cas);
  }

 protected:
  void Set(SetRequest request) {
    auto input = CommandInput::Make(request);
    Command set{request.id, "Set", input};

    NODE_LOG("Handle client request {} / {}", request, set);

    Execute(set);
    NODE_LOG("Client request {} / {} completed", request, set);
  }

  Value Get(GetRequest request) {
    auto input = CommandInput::Make(request);
    Command get{request.id, "Get", input};

    NODE_LOG("Handle client request {} / {}", request, get);

    auto output = Execute(get);
    NODE_LOG("Client request {} / {} completed", request, get);
    return output.As<Value>();
  }

  Value Cas(CasRequest request) {
    auto input = CommandInput::Make(request);
    Command cas{request.id, "Cas", input};

    NODE_LOG("Handle client request {} / {}", request, cas);

    auto output = Execute(cas);
    NODE_LOG("Client request {} / {} completed", request, cas);
    return output.As<Value>();
  }

 private:
  CommandOutput Execute(Command command) {
    // Submit command to Multi-Paxos
    auto future = rsm_->Execute(std::move(command));
    // Await Result<CommandOutput>
    auto result = Await(std::move(future));
    // Unpack CommandOutput
    auto output = result.Value();
    return output;
  }

 private:
  IReplicatedStateMachinePtr rsm_;
};

}  // namespace kv
