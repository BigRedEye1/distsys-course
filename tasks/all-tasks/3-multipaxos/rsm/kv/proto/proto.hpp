#pragma once

#include <rsm/kv/store/types.hpp>

#include <whirl/helpers/serialize.hpp>

#include <cereal/types/string.hpp>

#include <string>
#include <ostream>

namespace kv {

//////////////////////////////////////////////////////////////////////

// TODO: See `Client interaction` chapter from RAFT PhD
using RequestId = std::string;

//////////////////////////////////////////////////////////////////////

// Set(key, value)

struct SetRequest {
  RequestId id;
  Key key;
  Value value;

  WHIRL_SERIALIZE(CEREAL_NVP(id), CEREAL_NVP(key), CEREAL_NVP(value))
};

// Logging
std::ostream& operator<<(std::ostream& out, const SetRequest& set);

//////////////////////////////////////////////////////////////////////

// Get(key)

struct GetRequest {
  RequestId id;
  Key key;

  WHIRL_SERIALIZE(CEREAL_NVP(id), CEREAL_NVP(key))
};

// Logging
std::ostream& operator<<(std::ostream& out, const GetRequest& get);

//////////////////////////////////////////////////////////////////////

// Cas(key, expected_value, target_value)
// aka Compare-And-Set | Compare-And-Swap
// https://en.wikipedia.org/wiki/Compare-and-swap

struct CasRequest {
  RequestId id;
  Key key;
  Value expected_value;
  Value target_value;

  WHIRL_SERIALIZE(CEREAL_NVP(id), CEREAL_NVP(key), CEREAL_NVP(expected_value),
                  CEREAL_NVP(target_value))
};

// Logging
std::ostream& operator<<(std::ostream& out, const CasRequest& cas);

}  // namespace kv
