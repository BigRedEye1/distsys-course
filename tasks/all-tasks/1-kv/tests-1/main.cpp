#include <kv/types.hpp>
#include <kv/client/stub.hpp>
#include <kv/node/node.hpp>

// Simulation

#include <whirl/matrix/world/world.hpp>
#include <whirl/matrix/world/global/global.hpp>
#include <whirl/matrix/client/client.hpp>
#include <whirl/matrix/history/printers/kv.hpp>
#include <whirl/matrix/history/checker/check.hpp>
#include <whirl/matrix/history/models/kv.hpp>
#include <whirl/matrix/test/reporter.hpp>

#include <whirl/rpc/impl/id.hpp>
#include <await/fibers/core/id.hpp>

#include <random>

using namespace whirl;

//////////////////////////////////////////////////////////////////////

static const std::vector<std::string> kKeys({"a", "b", "c"});

const std::string& ChooseKey() {
  return kKeys.at(GlobalRandomNumber(GetGlobal<size_t>("num_keys")));
}

//////////////////////////////////////////////////////////////////////

class KVClient : public ClientBase {
 public:
  KVClient(NodeServices services) : ClientBase(std::move(services)) {
  }

 protected:
  void MainThread() override {
    kv::KVBlockingStub kv_store{Channel()};

    for (size_t i = 1;; ++i) {
      if (RandomNumber() % 2 == 0) {
        kv::Key key = ChooseKey();
        kv::Value value = RandomNumber(1, 100);
        NODE_LOG("Execute Set({}, {})", key, value);
        kv_store.Set(key, value);
        NODE_LOG("Set completed");
      } else {
        kv::Key key = ChooseKey();
        NODE_LOG("Execute Get({})", key);
        kv::Value result = kv_store.Get(key);
        NODE_LOG("Get({}) -> {}", key, result);
        (void)result;
      }

      Threads().SleepFor(RandomNumber(1, 100));
    }
  }
};

//////////////////////////////////////////////////////////////////////

using KVStoreModel = histories::KVStoreModel<kv::Key, kv::Value>;

//////////////////////////////////////////////////////////////////////

TestReporter reporter;

//////////////////////////////////////////////////////////////////////

// [3, 7]
size_t NumberOfReplicas(size_t seed) {
  return 3 + seed % 5;
}

// [1, 2]
size_t NumberOfKeys(size_t seed) {
  if (seed % 11 == 0) {
    return 2;
  }
  return 1;
}

// Seed -> simulation digest
size_t RunSimulation(size_t seed) {
  static const size_t kTimeLimit = 10000;
  static const size_t kCompletedCalls = 7;

  reporter.PrintSimSeed(seed);

  // Reset RPC and fiber ids
  await::fibers::ResetIds();
  whirl::rpc::ResetIds();

  World world{seed};

  // Cluster nodes
  auto node = MakeNode<kv::KVNode>();
  world.AddServers(NumberOfReplicas(seed), node);

  // Clients
  auto client = MakeNode<KVClient>();
  world.AddClients(3, client);

  // Globals
  world.SetGlobal("num_keys", NumberOfKeys(seed));

  // Log
  std::stringstream log;
  world.WriteLogTo(log);

  // Run simulation
  world.Start();

  while (world.NumCompletedCalls() < kCompletedCalls &&
      world.TimeElapsed() < kTimeLimit) {
    world.Step();
  }
  // Stop and compute simulation digest
  size_t digest = world.Stop();

  // Print report
  reporter.PrintSimReport(world);

  // Time limit exceeded
  if (world.NumCompletedCalls() < kCompletedCalls) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    reporter.PrintSimLog(log.str());
    reporter.Fail("Simulation time limit exceeded");
  }

  const auto history = world.History();
  reporter.DebugLine("History size: {}", history.size());

  const bool linearizable = histories::LinCheck<KVStoreModel>(history);

  if (!linearizable) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    reporter.PrintSimLog(log.str());
    reporter.PrintLine("History (seed = {}) is NOT LINEARIZABLE:", seed);
    reporter.PrintSimHistory<KVStoreModel>(history);
    reporter.Fail();
  }

  return digest;
}

//////////////////////////////////////////////////////////////////////

void RunSimulations(size_t count) {
  std::mt19937 seeds{42};

  reporter.ResetSimCount();
  reporter.PrintLine("Run simulations...");

  for (size_t i = 1; i <= count; ++i) {
    reporter.StartSim();
    RunSimulation(seeds());
    reporter.CompleteSim();
  }
}

//////////////////////////////////////////////////////////////////////

void TestDeterminism() {
  static const size_t kSeed = 104107713;

  reporter.PrintLine("Test determinism...");

  size_t digest = RunSimulation(kSeed);

  // Repeat with the same seed
  if (RunSimulation(kSeed) != digest) {
    reporter.Fail("Impl is not deterministic");
  } else {
    reporter.PrintLine("Ok");
  }
}

//////////////////////////////////////////////////////////////////////

int main() {
  TestDeterminism();
  RunSimulations(65536);

  reporter.Congratulate();

  return 0;
}
